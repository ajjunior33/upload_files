require('dotenv').config();
var aws = require("aws-sdk");
var express = require("express");
var multer = require("multer");
var mongoose = require("mongoose");

const multerConfig = require('./config/multers3');
const Post = require("./models/Post");

const app = express();
mongoose.connect(process.env.MONGO_URL, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
});


app.use(express.json());

app.get('/posts', async (request,response) => {
	const posts = await Post.find();
	return response.status(200).json({
		data: posts
	})
})

app.post(
	"/upload",
	multer(multerConfig).single("file"),
	async function (request, response, next) {
		try {
			const { originalname: name, size, key, location: url = "" } = request.file;

			const post = await Post.create({
				name,
				size,
				key,
				url,
			});

			return response.status(200).json(post);
		} catch (error) {
			console.log(error);
			return response.status(400).json({
				message: "Não foi possível fazer o upload do arquivo.",
			});
		}
	}
	);


app.delete('/remove/unique/:id', async (request,response) => {
	const { id } = request.params;

	const post = await Post.findById(id);

	if(!post){
		return response.status(400).json({message: "Post não encontrado."});
	}

	await post.remove();

	return response.status(200).json({
		message:"Deletado com sucesso."
	});
})

app.delete('/remove/multiple', async (request,response) =>{
	try{
		const { list } = request.body;
		var notDeleted = [];

		for ( const item of list){
			const post = await Post.findById(item);
			if(!post) notDeleted.push(item);

			if(post){
				post.remove();
			}
		}

		return response.status(200).json({
			message:"Processado com sucesso",
			notDeleted
		})
	}catch(error){
		console.log(error);
	}
})
module.exports = app;

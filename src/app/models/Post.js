const mongoose = require('mongoose');
const aws = require('aws-sdk');

const path = require('path');
const fs = require("fs");
const { promisify } = require('util')

const spacesEndpoint = new aws.Endpoint(process.env.AWS_DEFAULT_REGION);
const storageS3 = new aws.S3({
	accessKeyId: process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey:process.env.AWS_SECRET_ACCESS_KEY,
	endpoint: spacesEndpoint
});

const PostSchema = new mongoose.Schema({
	name: String,
	size: Number,
	key: String,
	url: String,
	createdAt:{
		type: Date,
		default: Date.now,
	},
});

PostSchema.pre('save', function(){
	if(!this.url){
		this.url = `${process.env.BASE_URL}/files/${this.key}`;
	}
})

PostSchema.pre('remove', function(){
	if(process.env.STORAGE_TYPE === 's3'){
		return storageS3.deleteObject({
			Bucket:process.env.AWS_BUCKET,
			Key: this.key
		}).promise();
	}else{
		return promisify(fs.unlink)(path.resolve(__dirname, '..','..','tmp','upload', this.key))
	}
})
module.exports = mongoose.model('Post', PostSchema);
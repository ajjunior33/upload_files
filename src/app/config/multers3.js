// require('dotenv').config();
const multer = require('multer');
const path = require('path');
const crypto = require('crypto');
const multerS3 = require('multer-s3');
const aws = require('aws-sdk');


const spacesEndpoint = new aws.Endpoint(process.env.AWS_DEFAULT_REGION);
const storageS3 = new aws.S3({
	accessKeyId: process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey:process.env.AWS_SECRET_ACCESS_KEY,
	endpoint: spacesEndpoint
})

const storageTypes = {
	local: multer.diskStorage({
		destination: (request, file, callback) => {
			callback(null,path.resolve(__dirname, '..','..','tmp','upload'));
		},

		filename: (request,file,callback) => {
			crypto.randomBytes(16, (err, hash) => {
				if(err) callback(err);
				file.key = `${hash.toString('hex')}-${file.originalname}`;
				callback(null, file.key);
			})
		}
	}),
	s3:multerS3({
		s3: storageS3,
		bucket: process.env.AWS_BUCKET,
		contentType: multerS3.AUTO_CONTENT_TYPE,
		acl: 'public-read',
		key: (request,file,callback) => {
			crypto.randomBytes(16, (err, hash) => {
				if(err) callback(err);

				const fileName = `${hash.toString('hex')}-${file.originalname}`;


				callback(null, fileName);

			})
		}
	})
}


module.exports ={

	// dest: path.resolve(__dirname, '..','..','tmp','upload'),
	storage: storageTypes[process.env.STORAGE_TYPE],
	limits:{
		fileSize: 2 * 1024 * 1024,
	},
	fileFilter: (request, file, callback) => {
		

		const allowedMimes = [
		'image/jpeg',
		'image/pjpeg',
		'image/png',
		'image/gif'
		];

		if(allowedMimes.includes(file.mimetype)){
			callback(null, true)
		}else{
			callback(new Error('Invalid file type.'));
		}
	},
}